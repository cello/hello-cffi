(in-package :cffi)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (export '(falloc)))

(defun deref-array (array type position)
  (mem-aref array type position))

(defun (setf deref-array) (value array type position)
  (setf (mem-aref array type position) value))

(defun falloc (type &optional (size 1))
  (cffi:foreign-alloc type :count size))

(defun free-foreign-object (ptr)
  (foreign-free ptr))