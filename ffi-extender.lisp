(in-package :cl-user)

(defpackage #:ffi-extender
  (:nicknames #:ffx)
  (:shadowing-import-from #:cffi #:with-foreign-object
    #:load-foreign-library #:with-foreign-string)
  (:use #:common-lisp #:cffi)
  (:export
   #:def-type
   #:def-foreign-type
   #:def-constant
   #:null-char-p
   #:def-enum
   #:def-struct
   #:get-slot-value
   #:get-slot-pointer
   #:def-array-pointer
   #:def-union
   #:allocate-foreign-object
   #:with-foreign-object
   #:with-foreign-objects
   #:size-of-foreign-type
   #:pointer-address
   #:deref-pointer
   #:ensure-char-character
   #:ensure-char-integer
   #:ensure-char-storable
   #:null-pointer-p
   #:+null-cstring-pointer+
   #:char-array-to-pointer
   #:with-cast-pointer
   #:def-foreign-var
   #:convert-from-cstring
   #:convert-to-cstring
   #:free-cstring
   #:with-cstring
   #:with-cstrings
   #:def-function
   #:find-foreign-library
   #:load-foreign-library
   #:default-foreign-library-type
   #:run-shell-command
   #:convert-from-foreign-string
   #:convert-to-foreign-string
   #:allocate-foreign-string
   #:with-foreign-string
   #:foreign-string-length              ; not implemented
   #:convert-from-foreign-usb8
   ))

(in-package :ffx)