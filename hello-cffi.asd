;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

;(declaim (optimize (debug 2) (speed 1) (safety 1) (compilation-speed 1)))
(declaim (optimize (debug 3) (speed 3) (safety 1) (compilation-speed 0)))


#-(or openmcl sbcl cmu clisp lispworks ecl allegro cormanlisp)
(error "Sorry, this Lisp is not yet supported.  Patches welcome!")

(asdf:defsystem :hello-cffi
  :name "Hello CFFI"
  :author "Kenny Tilton <ktilton@nyc.rr.com>"
  :version "1.0.0"
  :maintainer "Kenny Tilton <ktilton@nyc.rr.com>"
  :licence "Lisp Lesser GNU Public License"
  :description "CFFI Add-ons"
  :long-description "Extensions and utilities for CFFI"
  :depends-on (:cffi :cffi-uffi-compat)
  :serial t
  :components ((:file "my-uffi-compat")
               (:file "ffi-extender")
               (:file "definers")
               (:file "arrays")
               (:file "callbacks")))
